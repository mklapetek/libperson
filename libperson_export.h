#ifndef LIBPERSON_EXPORT_H
#define LIBPERSON_EXPORT_H

// needed for KDE_EXPORT and KDE_IMPORT macros
#include <kdemacros.h>

#ifndef LIBPERSON_EXPORT
# if defined(MAKE_LIBPERSON_LIB)
// We are building this library
#  define LIBPERSON_EXPORT KDE_EXPORT
# else
// We are using this library
#  define LIBPERSON_EXPORT KDE_IMPORT
# endif
#endif

# ifndef LIBPERSON_EXPORT_DEPRECATED
#  define LIBPERSON_EXPORT_DEPRECATED KDE_DEPRECATED LIBPERSON_EXPORT
# endif

#endif