/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Martin Klapetek <email>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <KDebug>

#include "persons-proxy-model.h"
#include "persons-model.h"

PersonsProxyModel::PersonsProxyModel(PersonsModel* personsModel, QObject* parent) : KCategorizedSortFilterProxyModel(parent)
{
    setSortCategoriesByNaturalComparison(true);
    setSourceModel(personsModel);
}

PersonsProxyModel::~PersonsProxyModel()
{

}

bool PersonsProxyModel::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const
{
    return QSortFilterProxyModel::filterAcceptsRow (source_row, source_parent);
}

bool PersonsProxyModel::subSortLessThan(const QModelIndex& left, const QModelIndex& right) const
{
    if (qobject_cast<PersonsModel*>(sourceModel())->modelType() == PersonsModel::AlphabetCategories) {
        return QString::localeAwareCompare(left.data(Qt::DisplayRole).toString(), right.data(Qt::DisplayRole).toString()) < 0;
    } else {
        //ExistingPerson < NotSavedPerson
        uint leftType = left.data(PersonsModel::PersonTypeRole).toUInt();
        uint rightType = right.data(PersonsModel::PersonTypeRole).toUInt();

        if (leftType < rightType) {
            return false;
        } else if (leftType > rightType) {
            return true;
        } else /*if (leftType == rightType)*/ {
            return QString::localeAwareCompare(left.data(Qt::DisplayRole).toString(), right.data(Qt::DisplayRole).toString()) < 0;
        }
    }

    return true;
}
