/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Martin Klapetek <email>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef PERSONS_PROXY_MODEL_H
#define PERSONS_PROXY_MODEL_H

#include "libperson_export.h"

#include <KCategorizedSortFilterProxyModel>

class PersonsModel;

class LIBPERSON_EXPORT PersonsProxyModel : public KCategorizedSortFilterProxyModel
{

public:
    explicit PersonsProxyModel(PersonsModel *personsModel, QObject *parent = 0);
    virtual ~PersonsProxyModel();

protected:
    virtual bool filterAcceptsRow (int source_row, const QModelIndex& source_parent) const;
//     virtual bool lessThan (const QModelIndex& left, const QModelIndex& right) const;
    virtual bool subSortLessThan(const QModelIndex& left, const QModelIndex& right) const;
};

#endif // PERSONS_PROXY_MODEL_H
