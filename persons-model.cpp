/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Martin Klapetek <email>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QPixmap>
#include <QPainter>
#include <QHash>
#include <QMultiHash>

#include <Nepomuk/Vocabulary/PIMO>
#include <Nepomuk/Vocabulary/NCO>
#include <Nepomuk/ResourceManager>

#include <nepomuk/simpleresource.h>
#include <nepomuk/simpleresourcegraph.h>
#include <nepomuk/datamanagement.h>

#include <Soprano/Model>
#include <Soprano/QueryResultIterator>
#include <Soprano/Vocabulary/NAO>

#include <KIconLoader>
#include <KMessageBox>
#include <KLocalizedString>
#include <KJob>
#include <KDebug>

//#include <libperson/person-object.h>
#include "person-object.h"

#include "persons-model.h"

#include <KCategorizedSortFilterProxyModel>
#include <Nepomuk/Resource>
#include <Nepomuk/Vocabulary/NFO>
#include <Nepomuk/Variant>
#include <Nepomuk/Vocabulary/NIE>

using namespace Nepomuk::Vocabulary;
using namespace Soprano::Vocabulary;

class PersonsModelPrivate {
    public:
        QList<QPair<PersonObject *, PersonsModel::PersonType> > data;
        PersonsModel::CategorizedModelType catModelType;
};

PersonsModel::PersonsModel(PersonsModel::CategorizedModelType type, QObject* parent)
    : QAbstractListModel(parent),
      d(new PersonsModelPrivate)
{
    d->catModelType = type;

    Nepomuk::ResourceManager::instance()->clearCache();

    QString query;
    query = "select distinct ?r ?l ?o ?p where { ?r a pimo:Person ."
                                          "OPTIONAL { ?r nao:prefLabel ?l . }"
                                          "?r pimo:groundingOccurrence ?o ."
                                          "OPTIONAL { ?r nao:prefSymbol ?p . }"
                                          "}";

    Soprano::QueryResultIterator it = Nepomuk::ResourceManager::instance()->mainModel()->executeQuery(query,
                                                                                                      Soprano::Query::QueryLanguageSparql);

    QHash<QUrl, QString> nameHash;
    QHash<QUrl, QString> photoHash;
    QMultiHash<QUrl, QUrl> occurenceHash;

    while(it.next()) {
        //kDebug() << Nepomuk::Resource(it["r"].uri()).allProperties();
        nameHash.insert(it["r"].uri(), it["l"].literal().toString());
        Nepomuk::Resource symbol(it["p"].uri());
        if (symbol.hasType(NFO::RasterImage())) {
            photoHash.insert(it["r"].uri(), symbol.property(NFO::fileUrl()).toResource().property(NIE::url()).toString());
            kDebug() << symbol.property(NFO::fileUrl()).toResource().property(NIE::url());
        }
        occurenceHash.insertMulti(it["r"].uri(), it["o"].uri());
    }

    QHashIterator<QUrl, QString> hashIter(nameHash);
    while (hashIter.hasNext()) {
        hashIter.next();

        PersonObject *person = new PersonObject(hashIter.value());
        person->setUri(hashIter.key());
        person->addContacts(occurenceHash.values(hashIter.key()));
        person->setPhoto(photoHash.value(hashIter.key()));

        foreach(const QUrl& uri, occurenceHash.values(hashIter.key())) {
            QList<QUrl> im = Nepomuk::Resource(uri).property(NCO::hasIMAccount()).toUrlList();
            if (im.isEmpty()) {
                continue;
            }
//             kDebug() << "IM found:" << im;
            person->addIMAccount(im);

//             QList<QUrl> affiliations = Nepomuk::Resource(uri).property(NCO::hasAffiliation()).toUrlList();
//             foreach(const QUrl& affUri, affiliations) {
//                 QList<QUrl> affPhonesList = Nepomuk::Resource(affUri).property(NCO::hasPhoneNumber()).toUrlList();
//                 foreach(const QUrl& affPhoneUri, affPhonesList) {
//                     person->addWorkPhone(Nepomuk::Resource(affPhoneUri).property(NCO::phoneNumber()).toString());
//                 }
//             }
        }

//         kDebug() << photoHash.value(hashIter.key());

        d->data.append(qMakePair<PersonObject*, PersonType>(person, PersonsModel::ExistingPerson));
    }
    //kDebug() << nameHash << occurenceHash;
}

PersonsModel::~PersonsModel()
{
    delete d;
}

QVariant PersonsModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    QPixmap pixmap;

    switch (role) {
        case Qt::DisplayRole:
            return d->data.at(index.row()).first->label();
        case Qt::DecorationRole:
            if (d->data.at(index.row()).first->photo().isNull()) {
                pixmap = KIconLoader::global()->loadIcon("user-identity", KIconLoader::NoGroup, 64);
            } else {
                if (d->data.at(index.row()).first->photo().width() >= d->data.at(index.row()).first->photo().height()) {
                    pixmap = d->data.at(index.row()).first->photo().scaledToWidth(64);
                } else {
                    pixmap = d->data.at(index.row()).first->photo().scaledToHeight(64);
                }
            }
            if (d->data.at(index.row()).second == PersonsModel::NotSavedPerson) {
                QPainter painter(&pixmap);
                KIcon("emblem-new").paint(&painter, 0, 0, 16, 16);
            }

            return pixmap;
        case KCategorizedSortFilterProxyModel::CategorySortRole:
            if (d->catModelType == PersonsModel::AlphabetCategories) {
                return d->data.at(index.row()).first->label().left(1).toUpper();
            } else if (d->catModelType == PersonsModel::SavedStateCategories) {
                if (d->data.at(index.row()).second == PersonsModel::ExistingPerson) {
                    //Put 'Persons to be stored' on the first position
                    return 2;
                } else {
                    return 1;
                }
            }
        case KCategorizedSortFilterProxyModel::CategoryDisplayRole:
            if (d->catModelType == PersonsModel::AlphabetCategories) {
                return d->data.at(index.row()).first->label().left(1).toUpper();
            } else if (d->catModelType == PersonsModel::SavedStateCategories) {
                if (d->data.at(index.row()).second == PersonsModel::ExistingPerson) {
                    return i18nc("Category headline in categorized view", "Persons in Nepomuk");
                } else {
                    return i18nc("Category headline in categorized view", "Persons to be stored");
                }
            }
        case PersonsModel::ItemRole:
            return QVariant::fromValue(d->data.at(index.row()).first);
        case PersonsModel::PersonTypeRole:
            return d->data.at(index.row()).second;
        default:
            return QVariant();
    }
}

int PersonsModel::rowCount(const QModelIndex& parent) const
{
    return d->data.size();
}

void PersonsModel::insertPerson(PersonObject* person)
{
    for (int i = 0; i < d->data.size(); i++) {
        if (d->data.at(i).first == person) {
            kDebug() << "Person already in model, emitting changed signal";
            d->data.replace(i, qMakePair<PersonObject*, PersonType>(person, PersonsModel::NotSavedPerson));
            emit dataChanged(createIndex(i, 0), createIndex(i, 0));
            return;
        }
    }

    beginInsertRows(QModelIndex(), d->data.size(), d->data.size());
    d->data.append(qMakePair<PersonObject*, PersonType>(person, PersonsModel::NotSavedPerson));
    endInsertRows();
}

void PersonsModel::savePersons()
{
    Nepomuk::SimpleResourceGraph g;
    for (int i = 0; i < d->data.size(); i++) {
        if (d->data.at(i).second == PersonsModel::ExistingPerson) {
            continue;
        }

        PersonObject *person = d->data.at(i).first;
        Nepomuk::SimpleResource personRes(person->uri());
        personRes.addType(PIMO::Person());

        if (!person->photoUrl().isEmpty()) {
            Nepomuk::SimpleResource photoRes;
            photoRes.addType(NFO::FileDataObject());
            photoRes.addType(NFO::RasterImage());
            photoRes.addType(NAO::Symbol());
            KUrl photoUrl(person->photoUrl());
            photoUrl.setProtocol("file://");
            kDebug() << "Contact has photo, url is" << photoUrl;
            photoRes.addProperty(NFO::fileUrl(), photoUrl);

            personRes.addProperty(NAO::prefSymbol(), photoRes.uri());

            g << photoRes;
        }

        if (!person->uri().isEmpty() && person->photoUrl().isEmpty()) {
            KJob *removePhotoJob = Nepomuk::removeProperties(QList<QUrl>() << person->uri(), QList<QUrl>() << NAO::prefSymbol());
            removePhotoJob->exec();
            kDebug() << "Removing photo";
        }

        foreach(QUrl uri, person->allContacts()) {
            Nepomuk::SimpleResource contactRes(uri);
            contactRes.addType(NCO::PersonContact());
            personRes.addProperty(PIMO::groundingOccurrence(), contactRes);

            g << contactRes;
        }

        personRes.addProperty(NAO::prefLabel(), person->label());

        g << personRes;
    }

    kDebug() << g;

    KJob *storeJob = Nepomuk::storeResources(g);
    storeJob->exec();

    if (storeJob->error()) {
        KMessageBox::detailedSorry(0,
                                   i18nc("Error dialog text", "Saving the data into Nepomuk failed. Please report this."),
                                   storeJob->errorString());
    } else {
        kDebug() << "Data stored in Nepomuk.";
        for (int i = 0; i < d->data.size(); i++) {
            if (d->data.at(i).second == PersonsModel::ExistingPerson) {
                continue;
            }

            PersonObject *person = d->data.at(i).first;
            d->data.replace(i, qMakePair<PersonObject*, PersonType>(person, PersonsModel::ExistingPerson));
        }
    }
}

void PersonsModel::setModelType (PersonsModel::CategorizedModelType type)
{
    d->catModelType = type;
}

PersonsModel::CategorizedModelType PersonsModel::modelType() const
{
    return d->catModelType;
}

bool PersonsModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    return false;
}
