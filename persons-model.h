/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Martin Klapetek <email>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef PREPARED_PERSONS_MODEL_H
#define PREPARED_PERSONS_MODEL_H

#include <QtCore/QAbstractItemModel>

#include "libperson_export.h"

class PersonObject;
class PersonsModelPrivate;

class LIBPERSON_EXPORT PersonsModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum PersonType {
        ExistingPerson,
        NotSavedPerson
    };
    enum Role {
        ItemRole = Qt::UserRole,
        PersonTypeRole,
        PersonImage
    };
    enum CategorizedModelType {
        AlphabetCategories,
        SavedStateCategories
    };
    explicit PersonsModel(PersonsModel::CategorizedModelType type, QObject *parent = 0);
    ~PersonsModel();

    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    void insertPerson(PersonObject *person);

    void setModelType(PersonsModel::CategorizedModelType type);
    PersonsModel::CategorizedModelType modelType() const;

public Q_SLOTS:
    void savePersons();

private:
    PersonsModelPrivate * const d;
};

#endif // PREPARED_PERSONS_MODEL_H
