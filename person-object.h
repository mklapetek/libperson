/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Martin Klapetek <email>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef PERSON_OBJECT_H
#define PERSON_OBJECT_H

#include <QObject>
#include <QMetaType>
#include <QUrl>
#include <QPixmap>

#include "libperson_export.h"

class PersonObjectPrivate;

class LIBPERSON_EXPORT PersonObject
{

public:
    PersonObject(const QString& label = QString());
    PersonObject(const PersonObject& other);
    ~PersonObject();

    QString label();
    void setLabel(const QString& label);

    QUrl uri();
    void setUri(const QUrl& uri);

    QPixmap photo();
    QUrl photoUrl();
    void setPhoto(const QPixmap& pixmap);
    void setPhoto(const QString& photoUrl);
    void setPhoto(const QUrl& photoUrl);
    void setPhotoUrl(const QString& photoUrl);
    void setPhotoUrl(const QUrl& photoUrl);
    void removePhoto();

    QList<QString> emails();
    void setEmails(QList<QString>& emails);
    void addEmail(const QString& email);
    void addEmail(const QList<QString>& emails);

    QList<QString> phones();
    void setPhones(QList<QString>& phones);
    void addPhone(const QString& phone);

    QList<QString> workPhones();
    void setWorkPhones(QList<QString>& workPhones);
    void addWorkPhone(const QString& workPhone);

    QList<QUrl> allContacts();
    void addContact(const QUrl& url);
    void removeContact(const QUrl& url);
    void addContacts(const QList<QUrl> contacts);

    QList<QUrl> allIMContacts();
    void addIMAccount(const QUrl& uri);
    void addIMAccount(const QList<QUrl>& uriList);

    QList<QUrl> affiliations();
    void addAffiliation(const QUrl& uri);
    void addAffiliation(const QList<QUrl>& uriList);

private:
    PersonObjectPrivate * const d;
};

typedef PersonObject * PersonObjectPtr;
Q_DECLARE_METATYPE(PersonObjectPtr)

#endif // PERSON_OBJECT_H
