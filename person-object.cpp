/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Martin Klapetek <email>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QPixmap>
#include <QList>
#include <QString>

#include "person-object.h"
#include <KDebug>

class PersonObjectPrivate
{
    public:
        QString             label;
        QUrl                uri;
        QList<QUrl>         contacts;
        QList<QUrl>         imUris;
        QList<QString>      emails;
        QList<QString>      phones;
        QList<QString>      workPhones;
        QPixmap             photo;
        QUrl                photoUrl;
        QList<QUrl>         affiliations;
};

PersonObject::PersonObject(const QString& label)
    :   d(new PersonObjectPrivate)
{
        d->label = label;
}

PersonObject::PersonObject(const PersonObject& other)
    : d(new PersonObjectPrivate)
{
    *d = *other.d;
}

PersonObject::~PersonObject()
{
    delete d;
}

void PersonObject::addContact(const QUrl& url)
{
    d->contacts << url;
}

void PersonObject::removeContact(const QUrl& url)
{
    d->contacts.removeAll(url);
}

QList<QUrl> PersonObject::allContacts()
{
    return d->contacts;
}

QString PersonObject::label()
{
    return d->label;
}

void PersonObject::setLabel(const QString& label)
{
    d->label = label;
}

QUrl PersonObject::uri()
{
    if (!d->uri.isEmpty()) {
        return d->uri;
    }

    return QUrl();
}

void PersonObject::setUri(const QUrl& uri)
{
    d->uri = uri;
}

void PersonObject::addContacts(const QList<QUrl> contacts)
{
    d->contacts.append(contacts);
}

QPixmap PersonObject::photo()
{
    return d->photo;
}

QUrl PersonObject::photoUrl()
{
    return d->photoUrl;
}

void PersonObject::setPhoto(const QPixmap& pixmap)
{
    d->photo = pixmap;
}

void PersonObject::setPhoto(const QString& photoUrl)
{
    d->photo = QPixmap(QString(photoUrl).remove("file://"));
    d->photoUrl = QUrl(photoUrl);
}

void PersonObject::setPhoto(const QUrl& photoUrl)
{
    kDebug() << photoUrl.toLocalFile();
    d->photo = QPixmap(photoUrl.toLocalFile().remove("file://"));
    kDebug() << d->photo.isNull();
    d->photoUrl = photoUrl;
}

void PersonObject::setPhotoUrl (const QString& photoUrl)
{
    d->photoUrl = QUrl(photoUrl);
}

void PersonObject::setPhotoUrl (const QUrl& photoUrl)
{
    d->photoUrl = photoUrl;
}

void PersonObject::removePhoto()
{
    d->photoUrl = QUrl();
    d->photo = QPixmap();
}

QList<QString> PersonObject::emails()
{
    return d->emails;
}

void PersonObject::setEmails(QList<QString>& emails)
{
    d->emails = emails;
}

void PersonObject::addEmail(const QString& email)
{
    d->emails.append(email);
}

void PersonObject::addEmail(const QList<QString>& emails)
{
    d->emails.append(emails);
}

QList<QString> PersonObject::phones()
{
    return d->phones;
}

void PersonObject::setPhones(QList<QString>& phones)
{
    d->phones = phones;
}

void PersonObject::addPhone (const QString& phone)
{
    d->phones.append(phone);
}

QList<QString> PersonObject::workPhones()
{
    return d->workPhones;
}

void PersonObject::setWorkPhones(QList<QString>& workPhones)
{
    d->workPhones = workPhones;
}

void PersonObject::addWorkPhone(const QString& workPhone)
{
    d->workPhones.append(workPhone);
}

QList<QUrl> PersonObject::allIMContacts()
{
    return d->imUris;
}

void PersonObject::addIMAccount(const QUrl& uri)
{
    d->imUris.append(uri);
}

void PersonObject::addIMAccount(const QList<QUrl>& uriList)
{
    d->imUris.append(uriList);
}

QList<QUrl> PersonObject::affiliations()
{
    return d->affiliations;
}

void PersonObject::addAffiliation(const QUrl& uri)
{
    d->affiliations.append(uri);
}

void PersonObject::addAffiliation(const QList<QUrl>& uriList)
{
    d->affiliations.append(uriList);
}
